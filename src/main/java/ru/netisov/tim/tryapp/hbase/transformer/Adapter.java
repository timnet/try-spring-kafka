package ru.netisov.tim.tryapp.hbase.transformer;

public interface Adapter {
    String kafkaName(String v1);

    String hBaseName(String v1);
}
