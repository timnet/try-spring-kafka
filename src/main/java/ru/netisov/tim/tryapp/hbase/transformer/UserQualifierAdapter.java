package ru.netisov.tim.tryapp.hbase.transformer;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserQualifierAdapter implements Adapter {
    private static final Map<String, String> kafkaValues = new HashMap<>();
    private static final Map<String, String> hbValues = new HashMap<>();

    public static final String USERNAME_Q = "name";
    public static final String PASSWORD_Q = "pass";
    public static final String MAIL_Q = "ema";


    public static final String USERNAME_Q_H = "user";
    public static final String PASSWORD_Q_H = "password";
    public static final String MAIL_Q_H = "email";

    static {
        kafkaValues.put(USERNAME_Q_H, USERNAME_Q);
        kafkaValues.put(PASSWORD_Q_H, PASSWORD_Q);
        kafkaValues.put(MAIL_Q_H, MAIL_Q);
    }

    static {
        hbValues.put(USERNAME_Q, USERNAME_Q_H);
        hbValues.put(PASSWORD_Q, PASSWORD_Q_H);
        hbValues.put(MAIL_Q, MAIL_Q_H);
    }


    @Override
    public String kafkaName(String v1) {
        return kafkaValues.get(v1);
    }

    @Override
    public String hBaseName(String v1) {
        return hbValues.get(v1);
    }
}
