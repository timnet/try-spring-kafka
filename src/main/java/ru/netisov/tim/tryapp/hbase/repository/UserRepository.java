package ru.netisov.tim.tryapp.hbase.repository;

import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.stereotype.Repository;
import ru.netisov.tim.tryapp.hbase.model.User;
import ru.netisov.tim.tryapp.hbase.transformer.UserQualifierAdapter;

import java.util.List;

@Repository
public class UserRepository {

    @Autowired
    private HbaseTemplate hbaseTemplate;

    private String tableName = "users";

    private static byte[] CF_INFO = Bytes.toBytes("cfInfo");

    private byte[] qUser = Bytes.toBytes(UserQualifierAdapter.USERNAME_Q_H);
    private byte[] qEmail = Bytes.toBytes(UserQualifierAdapter.MAIL_Q_H);
    private byte[] qPassword = Bytes.toBytes(UserQualifierAdapter.PASSWORD_Q);


    public List<User> findAll() {
        return hbaseTemplate.find(tableName, "cfInfo", (result, rowNum) ->
                new User(Bytes.toString(result.getValue(CF_INFO, qUser)),
                        Bytes.toString(result.getValue(CF_INFO, qEmail)),
                        Bytes.toString(result.getValue(CF_INFO, qPassword))));

    }


    public void save(final String username, final String value, final String qualifier) {
        hbaseTemplate.put(tableName, username, new String(CF_INFO), qualifier, Bytes.toBytes(value));
    }


    public void delete(final String row, final String qualifier) {
        hbaseTemplate.delete(tableName, row, new String(CF_INFO), qualifier);
    }


}
