package ru.netisov.tim.tryapp.consume;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import ru.netisov.tim.tryapp.hbase.cdc.model.HRow;
import ru.netisov.tim.tryapp.hbase.repository.UserRepository;
import ru.netisov.tim.tryapp.hbase.transformer.UserQualifierAdapter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Timofey Netisov <tnetisov@amt.ru>
 */
public class Listener {

    private static final String FAMILY = "cfUser";

    @Autowired
    private UserQualifierAdapter userQualifierAdapter;

    @Autowired
    private UserRepository userRepository;

    @KafkaListener(group = "test-consumer-group", topics = "test")
    public void listen1(HRow row) {
        Map<String, String> values = row.getColumns().stream()
                .filter(c -> new String(c.getFamily()).equals(FAMILY))
                .collect(HashMap::new, (h, c) -> h.put(new String(c.getQualifier()),
                        new String(c.getValue())), HashMap::putAll);
        String rowName = new String(row.getRowKey());
        if (row.getRowOp().equals(HRow.RowOp.DELETE)) {
            values.forEach((k, v) ->
                    userRepository.delete(rowName, userQualifierAdapter.hBaseName(k)));
        } else {
            values.forEach((k, v) -> userRepository.save(rowName, v, userQualifierAdapter.hBaseName(k)));
        }

        userRepository.findAll().forEach(System.out::println);

    }

}
